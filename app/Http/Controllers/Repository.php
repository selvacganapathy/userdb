<?php 

namespace App\Repositories;

abstract class Repository {

	public function transformCollection(array $items)
    {
    	return array_map([$this,'transform'],$items);
    }

    public abstract function transform($item);

}

?>