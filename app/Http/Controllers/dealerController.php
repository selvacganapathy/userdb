<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\userdb\Dealership;
use App\Models\Access\agency;
use App\Repositories\DealershipRepo;
use PSALogger;

class dealerController extends StatusController
{
    protected $DealershipRepo;

	function __construct(DealershipRepo $DealershipRepo)
    {
        $this->middleware('userAuth');

        $this->DealershipRepo = $DealershipRepo;

    }

    public function index() 
    {
        return $this->responseNotFound('Unauthorised Access',404);
    }

    public function show($dealercode) 
    {
        $dealerships = Dealership::where('code',$dealercode)->firstOrFail();

        return $this->response([

                    'data' => $dealerships->organisation->name
        ]);
    }

    public function location($dealercode) 
    {
        $dealership = array();
        
        $dealerships = Dealership::get();

        $getPostCode = Dealership::where('code',$dealercode)->firstOrFail();

        PSALogger::Activity(Dealership::where('code',$dealercode)->toSql());

        if(empty($getPostCode))
        {
            
            PSALogger::Error("No dealers found on this location");
            
            return $this->responseNotFound('No dealers found on this location',404);
        }

        foreach ($dealerships as $key => $dealer) 
        {
            if($dealer['address']['postcode'] == $getPostCode->address->postcode)
            {
                $dealer->dealerGroup;

                $dealer->address;

                $dealership[] = $dealer;
            }
        }

        return $this->response([

            $this->DealershipRepo

                        ->transformCollection($dealership)
        ]);
    }
}
