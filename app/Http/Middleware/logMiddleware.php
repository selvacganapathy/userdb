<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use App\Models\Access\Permissions;
use App\Http\Controllers\StatusController;

class logMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(StatusController $StatusController)
    {
        $this->StatusController = $StatusController;
    }

    public function handle($request, Closure $next)
    {
            $getIp             = Request::ip();
            $getPermission     = permissions::get();
            $getBrand          = Request::segment(3);
            $getEndpoint       = Request::segment(4);
            $getIpaddressLong  = ip2long($getIp);
            /**
             * find the user IP allowed 
             */
            foreach ($getPermission as $permissionkey => $permissionvalue) 
            {
                if($permissionvalue->endpoint->endpoint_name == $getEndpoint && $permissionvalue->brand->brand_short_name == $getBrand)
                {
                    if($permissionvalue->ip_address->ip_low_long <= $getIpaddressLong && $permissionvalue->ip_address->ip_high_long >= $getIpaddressLong)
                    {
                        return $next($request);                    
                    }
                }
            }
            return $this->StatusController

                    ->responseNotFound('Unauthorised',401);    
    }
}
