<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Access\Apikey;
use App\Models\userdb\Dealership;
use App\Models\Access\Permissions;
use Request;
use App\Http\Controllers\StatusController;
use PSALogger;

class userdbAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(StatusController $StatusController)
    {
        $this->StatusController = $StatusController;
    }

    public function handle($request, Closure $next)
    {
        $headers_sent = getallheaders();
        
        if(empty($headers_sent['Authorization']))
        {
            return $this->StatusController

                            ->responseNotFound('Unauthorised Access',404);
        }
        
        $agencyType = apikey::where('key',$headers_sent['Authorization'])

                            ->first();

        if(empty($agencyType))
        {

            return $this->StatusController

                            ->responseNotFound('API key Invalid',404);
        }
        else
        {
            // return $next($request);
            $getIp             = Request::ip();
            $getPermission     = permissions::where('agency_id',$agencyType->agency_id)->get();
            $getBrand          = Request::segment(3);
            $getEndpoint       = Request::segment(4);
            $getIpaddressLong  = ip2long($getIp);
            /**
             * find the user IP allowed 
             */
            foreach ($getPermission as $permissionkey => $permissionvalue) 
            {
                if($permissionvalue->endpoint->endpoint_name == $getEndpoint && $permissionvalue->brand->brand_short_name == $getBrand)
                {
                    if($permissionvalue->ip_address->ip_low_long <= $getIpaddressLong && $permissionvalue->ip_address->ip_high_long >= $getIpaddressLong)
                    {
                    PSALogger::Access("Access Granted For: ".$getIp." Brand: ".$getBrand." Endpoint: ".$getEndpoint." Token: ".$headers_sent['Authorization']);
                        return $next($request);                    
                    }
                }
            }
            return $this->StatusController

                    ->responseNotFound('Access credentials invalid',404);

        }
    }
}
