<?php

/**
 * selva chinnakalai dated on Mar 2016
 * Restful API development for extended dealer DB 
 * Developer Notes: Implemented Respository pattern and gateway pattern is implemented in the composer package
 */
Route::group(['prefix' => 'api/v1/'.$segment],function($brand){
	
Route::get('/log','\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	/**
	Extended Dealer API
	| /api/extended_dealer/<dealer_code>/get/<key>                                 -> <value(s)>
	| /api/extended_dealer/<dealer_code>/set/<key>/value/<value>                   -> <boolean>
	| /api/extended_dealer/<dealer_code>/set_array/<key>/values/<keys,values>      -> <boolean>
	| /api/extended_dealer/<dealer_code>/delete/<key>                              -> <boolean>
	 */
	Route::group(['prefix' => 'extended_dealer'],function(){

		Route::get('/','dealerController@index'); 									/* no access */
		Route::get('get/dealer/{dealercode}','dealerController@show');				/* get the brand only */
		Route::get('get/location/{dealercode}','dealerController@location');		/* get all the dealer with in same location distinct of dealer group. except-> RD  */
	});
	/**
	Extended User API
	| /api/extended_user/<user_id>/get/<key>                                       -> <value(s)>
	| /api/extended_user/<user_id>/set/<key>/value/<value>                         -> <boolean>
	| /api/extended_user/<user_id>/set_array/<key>/values/<keys,values>            -> <boolean>
	| /api/extended_user/<user_id>/delete/<key>                                    -> <boolean>
	 */
	Route::group(['prefix' => 'extended_user'],function(){

		Route::get('/','userController@index');
		Route::get('/{userid}/get/system/all','userController@index');
		Route::get('/{userid}/get/system/all','userController@index');
	
	});
	/**
	Extended System API
	| /api/extended_system/<system_id>/get/<key>                                   -> <value(s)>
	| /api/extended_system/<system_id>/set/<key>/value/<value>                     -> <boolean>
	| /api/extended_system/<system_id>/set_array/<key>/values/<keys,values>        -> <boolean>
	| /api/extended_system/<system_id>/delete/<key>                                -> <boolean>
	**/
	Route::group(['prefix' => 'extended_system'],function(){

	Route::get('/','systemController@index');
	
	});

});
