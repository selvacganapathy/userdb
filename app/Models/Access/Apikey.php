<?php

namespace App\Models\Access;


use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class Apikey extends Model
{
	/**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
	use Eloquence, Mappable;

	protected $connection = "apiaccess";

    protected $table = "api_key";

    protected $primaryKey = 'id';
    protected $maps =[
        // simple alias

    ];
    protected $fillable = [

    ];

}
