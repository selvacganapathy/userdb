<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	/**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
	protected $connection = "apiaccess";

    protected $table = "brand";

    protected $primaryKey = 'id';
    
    protected $fillable = [


    ];
}
