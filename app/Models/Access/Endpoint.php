<?php

namespace App\Models\Access;


use Illuminate\Database\Eloquent\Model;

class Endpoint extends Model
{
	/**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */

	protected $connection = "apiaccess";

    protected $table = "endpoint";

    protected $primaryKey = 'id';
    
    protected $fillable = [


    ];
}
