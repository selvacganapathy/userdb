<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class Permissions extends Model
{
    use Eloquence, Mappable;
	/**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
	protected $connection = "apiaccess";

    protected $table = "permissions";

    protected $primaryKey = 'id';
    
    protected $maps =[

        // simple alias
        'endpoint_id' => 'fk_endpoint',
        'agency_id' => 'fk_agency',


    
    ];

    protected $fillable = [
    		'fk_endpoint',
    		'fk_agency',



    ];

    public function endpoint()
    {
        return $this->belongsTo(Endpoint::class);
    }
    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }
    public function ip_address()
    {
        return $this->belongsTo(Ipaddress::class);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    public function api_key()
    {
        return $this->belongsTo(Apikey::class);
    }
}
