<?php

namespace App\Models\userdb;

use Illuminate\Database\Eloquent\Model;

class Dealership extends Model
{
    /**
	 * $connection - mysql PDO connection driver to databases [usedb,pmcVinReg,cukVinReg]
	 * $primaryKey - primary key of the table [id - CHAR]
	 * 
	 */
    protected $connection = "mysql";
    
    protected $primaryKey = "id";

    protected $table = "dealerships";
    
    protected $fillable = [


    ];

    protected function organisation()
    {
        return $this->belongsTo('App\Models\userdb\Organisation');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\userdb\Addresses');
    }

    public function dealerGroup()
    {
        return $this->belongsTo('App\Models\userdb\Dealergroups');
    }

    public function regions()
    {
        return $this->belongsTo('App\Models\userdb\Regions');
    }
}
