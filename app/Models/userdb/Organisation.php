<?php

namespace App\Models\userdb;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
     /**
     * $connection - mysql PDO connection driver to databases [usedb,pmcVinReg,cukVinReg]
     * $primaryKey - primary key of the table [id - CHAR]
     * 
     */
    protected $connection = "mysql";
    
    protected $primaryKey = "id";

    protected $table = "organisations";

    protected $fillable = [
		'name',


    ];
}
