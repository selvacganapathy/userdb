<?php

namespace App\Models\userdb;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    /**
     * $connection - mysql PDO connection driver to databases
     * $primaryKey - primary key of the table
     * 
     */
    protected $connection = "mysql";
    
    protected $primaryKey = "id";

    protected $table = "addresses";

    protected $fillable = [


    ];

}
