<?php

namespace App\Models\userdb;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
	 * $connection - mysql PDO connection driver to databases
	 * $primaryKey - primary key of the table
	 * 
	 */
    protected $connection = "mysql";
    
    protected $primaryKey = "id";

    protected $table = "users";

    protected $fillable = [


    ];
}
